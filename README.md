# 3. Ödev [Tüm Şubeler için]

## Teslim Tarihi

```
21 Nisan 2019 (Pazar günü saat 23:59’a kadar.)
```
## Ödev İçeriği

Yapmış olduğunuz 2. Ödevi C dilini kullanarak tekrar yazınız. Burada dikkat edilmesi gereken Lab
kısmında gösterildiği gibi nesne yönelimliliğe benzetim yapılarak kod yazılmalıdır. Temelde beklenen
dosyalar

- Random.h Random.c // Rastgele sayı üreten yapı
- RastgeleKarakter.h RastgeleKarakter.c // Rastgele karakter üreten yapı
- Test.c // Geliştirilen yapıyı test eden program.

Burada RastgeleKarakter, Random yapısından **kalıtım alıyormuş** (Lab’ta gösterildiği şekli ile) gibi
benzetim yapılmalıdır. Yani Random yapısından yardım alıp rastgele karakter üretecektir.

Yazılan fonksiyonlar yapı içerisinden **fonksiyon göstericisi** yardımıyla gösterilmeli ve o şekilde
çağrılmalıdır (Lab örneğine bakınız).

**Başlık dosyalarında metot gövdesi yazılmamalıdır.**

Beklenen test detaylı bir testtir. Yani bir karakter üretilecekse 100 adet “bir karakter üretip” ekrana
yazılmalıdır ki gerçekten rastgele üretilip üretilmediği test edilebilsin. Kullanıcıdan herhangi bir girdi
alınmayacak bütün test bir kerede ekrana basılıp program sonlanacaktır.

Random yapısı içerisinde 2. ödevdeki gibi rastgele sayıyı hazır fonksiyon ile değil kendiniz
sağlamalısınız.

**Yardımcı olması açısından birkaç örnek**

Rastgele Karakter: a
Rastgele Karakter: p
Rastgele 3 Karakter: Lhr
Rastgele 3 Karakter: yuE
Verilen iki karakter (a,k): b
Verilen iki karakter (a,k): hj
Belirtilen Karakterler (g,y,u,c,n,e): e
Belirtilen Karakterler (g,y,u,c,n,e): u
Belirtilen Karakterler (g,y,u,c,n,e): cg

**Kodlar makefile yardımıyla derlenmelidir. MinGW C dilinde derlenmeyen kod kabul edilmez.**

### Önemli Not: Raporunuz detaylı olmalı ve kendi cümleleriniz olmalıdır (Örnek rapor için

### http://ebs.sabis.sakarya.edu.tr/DersTumDetay/tr/2016/255/21/2/50107/0 ). Kopya ödevler sıfır

### olarak değerlendirilecektir. SABİS şifreniz sizin sorumluluğunuz altındadır eğer arkadaşınız

### sizden habersiz ödevinizi alırsa bundan sizde sorumlu tutulur ve sıfır alırsınız.


### ÖDEV GRUP OLARAK YAPILABİLİR, GRUPLAR EN FAZLA 2 KİŞİDEN OLUŞABİLİR. HERKES

### KENDİ ŞUBESİ İLE GRUP OLUŞTURABİLİR. TANIDIĞINIZ YOKSA ÖDEVİ BİREYSEL

### YAPMALISINIZ.

## Teslim Formatı

```
Ödevi klasörün adı öğrenci numaranız ve bütün klasörler içinde olacak şekilde .rar’lı bir şekilde
sıkıştırıp http:///www.csodev.sakarya.edu.tr adresi üzerinden gönderiniz. Yukarıda belirtilen
teslim tarihinden sonra ödevler kesinlikle kabul edilmeyecektir.
```
```
Rapor pdf formatında olmalıdır. Raporu ayrıca çıktı olarak getirmenize gerek yoktur. Raporunuzda
kısaca sizden istenilen, öğrendikleriniz, ödevde yaptıklarınız, eksik bıraktığınız yerler, zorlandığınız
kısımlar anlatılabilir. Ödev raporunda yazı boyutu 12 puntodan büyük olamaz ve en az 1 sayfa en
çok 2 sayfa (kapak hariç) olabilir.
```
### KOPYA ÖDEV SIFIR OLARAK DEĞERLENDİRİLMEKTEDİR


