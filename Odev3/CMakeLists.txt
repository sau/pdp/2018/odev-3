cmake_minimum_required(VERSION 3.14)
project(Odev3 C)

set(CMAKE_C_STANDARD 11)

include_directories(include)

add_executable(Odev3 test/test.c include/random.h src/random.c include/util.h src/util.c include/rastgele-karakter.h src/rastgele-karakter.c)