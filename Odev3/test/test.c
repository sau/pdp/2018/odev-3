#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "util.h"
#include "rastgele-karakter.h"

extern void print_random_letter(RastgeleKarakter, unsigned int);

extern void print_many_letters(RastgeleKarakter, unsigned int, unsigned int);

extern void print_one_of_two(RastgeleKarakter, char, char, unsigned int);

extern void print_many_of_two(RastgeleKarakter, char, char, unsigned int, unsigned int);

extern void print_one_of_many(RastgeleKarakter, const char *, unsigned int);

extern void print_many_of_many(RastgeleKarakter, const char *, unsigned int, unsigned int);

int main() {
    RastgeleKarakter rk = new_rastgele_karakter();

    const unsigned int repeats = 10;

    print_random_letter(rk, repeats);
    printf("\n");

    print_many_letters(rk, 3, repeats);
    printf("\n");

    print_one_of_two(rk, 'a', 'k', repeats);
    printf("\n");

    print_many_of_two(rk, 'a', 'k', 2, repeats);
    printf("\n");

    print_one_of_many(rk, "gyucne", repeats);
    printf("\n");

    print_many_of_many(rk, "gyucne", 2, repeats);

    free_rastgele_karakter(rk);

    return 0;
}

inline void print_random_letter(RastgeleKarakter rk, unsigned int repeats) {
    for (int i = 0; i < repeats; ++i) {
        printf("%d. Rastgele karakter: %c\n", i + 1, rk->get_letter(rk));
    }
}

inline void print_many_letters(RastgeleKarakter rk, unsigned int count, unsigned int repeats) {
    char *buffer = malloc(sizeof(char) * (count + 1));

    for (int i = 0; i < repeats; ++i) {
        printf("%d. Rastgele %d karakter: %s\n", i + 1, count, rk->get_letters(rk, buffer, count));
    }

    free(buffer);
}

inline void print_one_of_two(RastgeleKarakter rk, char a, char b, unsigned int repeats) {
    char str[] = {a, b, '\0'};

    for (int i = 0; i < repeats; ++i) {
        printf("%d. Verilen 2 karakterden 1 tane (%c,%c): %c\n", i + 1, a, b, rk->pick_one_letter(rk, str));
    }
}

inline void print_many_of_two(RastgeleKarakter rk, char a, char b, unsigned int count, unsigned int repeats) {
    char str[] = {a, b, '\0'};
    char *buffer = malloc(sizeof(char) * (count + 1));

    for (int i = 0; i < repeats; ++i) {
        printf("%d. Verilen 2 karakterden %d tane (%c,%c): %s\n", i + 1, count, a, b,
               rk->pick_multiple_letters(rk, str, buffer, count));
    }

    free(buffer);
}

inline void print_one_of_many(RastgeleKarakter rk, const char *str, unsigned int repeats) {
    char *commas = malloc(sizeof(char) * 2 * strlen(str));
    get_comma_separated_string(str, commas);

    for (int i = 0; i < repeats; ++i) {
        printf("%d. Belirtilen karakterlerden 1 tane (%s): %c\n", i + 1, commas, rk->pick_one_letter(rk, str));
    }

    free(commas);
}

inline void print_many_of_many(RastgeleKarakter rk, const char *str, unsigned int count, unsigned int repeats) {
    char *commas = malloc(sizeof(char) * 2 * strlen(str));
    char *buffer = malloc(sizeof(char) * (count + 1));
    get_comma_separated_string(str, commas);

    for (int i = 0; i < repeats; ++i) {
        printf("%d. Belirtilen karakterlerden %d tane (%s): %s\n", i + 1, count, commas,
               rk->pick_multiple_letters(rk, str, buffer, count));
    }

    free(commas);
    free(buffer);
}
