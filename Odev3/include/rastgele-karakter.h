#ifndef ODEV3_RASTGELE_KARAKTER_H
#define ODEV3_RASTGELE_KARAKTER_H


#include "random.h"

typedef struct rastgele_karakter {
    /* Üye değişkenler */
    Random random;


    /* Üye fonksiyon bildirileri */
    /**
     * @param this Rastgele karakteri üretecek olan yapı
     * @return Rastgele bir harf
     */
    char (*get_letter)(struct rastgele_karakter *this);

    /**
     * @param this Rastgele harfleri üretecek olan yapı
     * @param dest Harflerin yazılacağı hedef dizi, yeterince büyük olmalıdır
     * @param count Dizinin başından itibaren kaç adet harf oluşturulacağı
     * @return Hedef diziyi döndürür
     */
    char *(*get_letters)(struct rastgele_karakter *this, char *dest, size_t count);

    /**
     * @param this Rastgele seçimi yapacak olan yapı
     * @param letters İçinden rastgele seçim yapılacak olan karakter katarı
     * @return Rastgele seçilen karakter
     */
    char (*pick_one_letter)(struct rastgele_karakter *this, const char *letters);

    /**
     * @param this Rastgele seçimleri yapacak olan yapı
     * @param letters İçinden rastgele seçim yapılacak olan karakter katarı
     * @param dest Seçilenlerin yazılacağı hedef dizi, yeterince büyük olmalıdır
     * @param count Kaç adet rastgele karakter seçileceği
     * @return Hedef diziyi döndürür
     */
    char *(*pick_multiple_letters)(struct rastgele_karakter *this, const char *letters, char *dest, size_t count);
} *RastgeleKarakter;


/* Global fonksiyon bildirileri */
extern RastgeleKarakter new_rastgele_karakter();

extern void free_rastgele_karakter(RastgeleKarakter);


#endif //ODEV3_RASTGELE_KARAKTER_H
