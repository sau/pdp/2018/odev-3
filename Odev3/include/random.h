#ifndef ODEV3_RANDOM_H
#define ODEV3_RANDOM_H

#include <stdbool.h>

typedef struct random {
    unsigned long seed;

    /**
     * @param this Sayıyı üretecek olan random yapısı
     * @param max Sınır değeri, 0 ya da daha küçükse sınır yoktur
     * @return Rastgele bir sayı
     */
    int (*get_number)(struct random *this, int max);

    /**
     * @param this Değeri üretecek olan random yapısı
     * @return Rastgele true ya da false
     */
    bool (*get_bool)(struct random *this);
} *Random;

extern Random new_random();

extern void free_random(Random random);

#endif //ODEV3_RANDOM_H
