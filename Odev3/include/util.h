#ifndef ODEV3_UTIL_H
#define ODEV3_UTIL_H

#include <time.h>
#include <math.h>

/**
 * @return Şu anki zaman, milisaniye olarak
 */
extern long get_current_time_millis();

/**
 * Kaynak dizideki tüm karakterlerin arası virgülle ayrılmış yeni bir dizi oluşturur
 * @param src Kaynak karakter katarı
 * @param dest Hedef karakter katarı, yeterli boyutta olmalıdır
 * @return Hedef dizi
 */
extern char *get_comma_separated_string(const char *src, char *dest);

#endif //ODEV3_UTIL_H
