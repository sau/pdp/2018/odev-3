#include <string.h>
#include "util.h"

long get_current_time_millis() {
    long ms; // Milliseconds
    struct timespec spec;

    clock_gettime(CLOCK_REALTIME, &spec);

    ms = (long) round(spec.tv_nsec / 1e6); // Convert nanoseconds to milliseconds

    return ms;
}

char *get_comma_separated_string(const char *src, char *dest) {
    size_t last = strlen(src) - 1;

    for (int i = 0, j = 2 * i; i < last; ++i, j = 2 * i) {
        dest[j] = src[i];
        dest[j + 1] = ',';
    }

    size_t s = 2 * last;
    dest[s] = src[last];
    dest[s + 1] = '\0';

    return dest;
}
