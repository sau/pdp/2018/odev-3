#include <stdlib.h>
#include <string.h>
#include "rastgele-karakter.h"

/* Üye fonksiyon bildirileri */
extern char get_letter(struct rastgele_karakter *);

extern char *get_letters(struct rastgele_karakter *, char *, size_t);

extern char pick_one_letter(struct rastgele_karakter *, const char *);

extern char *pick_multiple_letters(struct rastgele_karakter *, const char *, char *, size_t);


/* Dışa kapalı fonksiyon tanımları */
static inline char _pick_one_letter(struct rastgele_karakter *this, const char *letters, size_t size) {
    Random random = this->random;
    int index = random->get_number(random, size);

    return letters[index];
}


/* Global fonksiyon tanımları */
RastgeleKarakter new_rastgele_karakter() {
    RastgeleKarakter rk = malloc(sizeof(struct rastgele_karakter));

    rk->random = new_random();
    rk->get_letter = &get_letter;
    rk->get_letters = &get_letters;
    rk->pick_one_letter = &pick_one_letter;
    rk->pick_multiple_letters = &pick_multiple_letters;

    return rk;
}

void free_rastgele_karakter(RastgeleKarakter rk) {
    free_random(rk->random);
    free(rk);
}


/* Üye fonksiyon tanımları */
char get_letter(struct rastgele_karakter *this) {
    Random random = this->random;
    if (random->get_bool(random)) {
        return random->get_number(this->random, 'z' - 'a' + 1) + 'a';
    } else {
        return random->get_number(this->random, 'Z' - 'A' + 1) + 'A';
    }
}

char *get_letters(struct rastgele_karakter *this, char *dest, size_t count) {
    for (int i = 0; i < count; ++i) {
        dest[i] = this->get_letter(this);
    }

    dest[count] = '\0';

    return dest;
}

char pick_one_letter(struct rastgele_karakter *this, const char *letters) {
    size_t size = strlen(letters);

    return _pick_one_letter(this, letters, size);
}

char *pick_multiple_letters(struct rastgele_karakter *this, const char *letters, char *dest, size_t count) {
    size_t size = strlen(letters);

    for (int i = 0; i < count; ++i) {
        dest[i] = _pick_one_letter(this, letters, size);
    }

    dest[count] = '\0';

    return dest;
}
