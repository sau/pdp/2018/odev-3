#include <stdlib.h>
#include "random.h"
#include "util.h"

/* Üye fonksiyon bildirileri */
extern int get_number(struct random *, int);

extern bool get_bool(struct random *);

/* Global fonksiyon tanımları */
Random new_random() {
    Random random = malloc(sizeof(struct random));
    random->seed = get_current_time_millis(); /* İlk besleme değeri olarak geçen milisaniye değeri alınır */
    random->get_number = &get_number;
    random->get_bool = &get_bool;
    return random;
}

void free_random(Random random) {
    free(random);
}

/* Üye fonksiyon tanımları */
int get_number(struct random *this, int max) {
    this->seed = (((this->seed * 214013 + 2531011) >> 16) & 32767);
    long number = this->seed;

    return (max > 0 ? number % max : number);
}

bool get_bool(struct random *this) {
    long num1 = this->get_number(this, 0);
    long num2 = this->get_number(this, 0);

    return num1 > num2;
}
